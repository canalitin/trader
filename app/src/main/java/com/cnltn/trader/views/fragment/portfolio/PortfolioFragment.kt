package com.cnltn.trader.views.fragment.portfolio

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.cnltn.trader.R
import com.cnltn.trader.adapters.AdpUserPortfolio
import com.cnltn.trader.databinding.FragPortfolioBinding
import com.cnltn.trader.models.model.UrlParameter
import com.cnltn.trader.models.model.UserPortfolio
import com.cnltn.trader.utilities.format
import com.cnltn.trader.utilities.showMessage
import com.cnltn.trader.views.bases.BaseFragment

class PortfolioFragment : BaseFragment() {

    private lateinit var bind: FragPortfolioBinding
    private lateinit var viewModel: PortfolioViewModel
    private val args: PortfolioFragmentArgs by navArgs()
    private lateinit var params: UrlParameter
    private var adpUserPortfolio = AdpUserPortfolio()

    private val portfolioObserver = Observer<UserPortfolio> {
        onPortfolioSuccess(it)
    }

    private val errorObserver = Observer<String> {
        onFailed(it)
        bind.swipeRefresh.isRefreshing = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            exitControl()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.frag_portfolio, container, false)
        return bind.root
    }

    override fun initUI() {
        prepareAll()
        swipeControl()
        getPortfolio()
    }

    override fun initObservers() {
        viewModel.userPortfolio.observe(viewLifecycleOwner, portfolioObserver)
        viewModel.errorResult.observe(viewLifecycleOwner, errorObserver)
    }

    private fun prepareAll() {
        viewModel = ViewModelProvider(this).get(PortfolioViewModel::class.java)
        bind.portfolioList.adapter = adpUserPortfolio
        bind.viewModel = viewModel
        params = args.params
        params.setDefaultParameterForPortfolio()
    }

    private fun swipeControl() {
        bind.swipeRefresh.setOnRefreshListener {
            getPortfolio()
        }
    }

    private fun getPortfolio() {
        bind.swipeRefresh.isRefreshing = true
        viewModel.getPortfolio(params)
    }

    private fun onPortfolioSuccess(userPortfolio: UserPortfolio) {
        if (userPortfolio.result.state) {
            adpUserPortfolio.setItemList(userPortfolio.items)
            setTotalPrice()
        } else {
            context.showMessage(
                getString(R.string.msg_title_login_fail),
                userPortfolio.result.description
            )
        }
        bind.swipeRefresh.isRefreshing = false
    }

    private fun setTotalPrice() {
        bind.tvTotalPrice.text = viewModel.getTotalPriceOfPortfolio().format()
    }

    private fun exitControl() {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(R.string.msg_title_alert)
        alertDialog.setMessage(getString(R.string.msg_exit))
        alertDialog.setPositiveButton(R.string.btn_yes) { dialog, _ ->
            dialog.dismiss()
            this.activity?.finish()
        }
        alertDialog.setNegativeButton(R.string.btn_no) { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }
}