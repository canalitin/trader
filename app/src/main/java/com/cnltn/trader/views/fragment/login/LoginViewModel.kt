package com.cnltn.trader.views.fragment.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cnltn.trader.models.enum.Tag
import com.cnltn.trader.models.model.UrlParameter
import com.cnltn.trader.models.model.User
import com.cnltn.trader.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class LoginViewModel : ViewModel() {

    private val disposable = CompositeDisposable()
    private val apiService = ApiService()
    var user = MutableLiveData<User>()
    var errorResult = MutableLiveData<String>()

    fun getLoginIn(params: UrlParameter) {
        disposable.add(
            apiService.getUserLoginIn(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<User>() {
                    override fun onSuccess(t: User) {
                        user.value = t
                    }

                    override fun onError(e: Throwable) {
                        errorResult.value = e.localizedMessage.orEmpty()
                    }
                })
        )
    }
}