package com.cnltn.trader.views.fragment.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cnltn.trader.R
import com.cnltn.trader.databinding.FragLoginBinding
import com.cnltn.trader.models.model.UrlParameter
import com.cnltn.trader.models.model.User
import com.cnltn.trader.utilities.showMessage
import com.cnltn.trader.views.bases.BaseFragment

class LoginFragment : BaseFragment() {

    private lateinit var bind: FragLoginBinding
    private lateinit var viewModel: LoginViewModel
    private var params = UrlParameter()

    private val loginObserver = Observer<User> {
        onLoginSuccess(it)
    }

    private val errorObserver = Observer<String> {
        onFailed(it)
        bind.swipeRefresh.isRefreshing = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.frag_login, container, false)
        bind.login = this
        bind.params = params
        bind.swipeRefresh.isEnabled = false
        return bind.root
    }

    override fun initUI() {
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        params.setDefaultParameterForLoginIn()
    }

    override fun initObservers() {
        viewModel.user.observe(viewLifecycleOwner, loginObserver)
        viewModel.errorResult.observe(viewLifecycleOwner, errorObserver)
    }

    private fun onLoginSuccess(user: User) {
        if (user.result.state) {
            params.accountID = user.defaultAccount
            navToPortfolioFragment()
        } else {
            context.showMessage(
                getString(R.string.msg_title_login_fail),
                user.result.description
            )
        }
        bind.swipeRefresh.isRefreshing = false
    }

    fun loginIn() {
        if (!params.username.isNullOrEmpty() && !params.password.isNullOrEmpty()) {
            bind.swipeRefresh.isRefreshing = true
            viewModel.getLoginIn(params)
        } else {
            context.showMessage(
                getString(R.string.msg_title_login_fail),
                getString(R.string.msg_empty_username_or_password)
            )
        }
    }

    private fun navToPortfolioFragment() {
        val action = LoginFragmentDirections.actionLoginToPortfolio(params)
        navigate(action)
    }

}