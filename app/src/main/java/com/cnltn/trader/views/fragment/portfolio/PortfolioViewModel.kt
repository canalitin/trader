package com.cnltn.trader.views.fragment.portfolio

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cnltn.trader.models.model.UrlParameter
import com.cnltn.trader.models.model.UserPortfolio
import com.cnltn.trader.services.ApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class PortfolioViewModel : ViewModel() {

    private var disposable = CompositeDisposable()
    private val apiService = ApiService()

    var userPortfolio = MutableLiveData<UserPortfolio>()
    var errorResult = MutableLiveData<String>()

    fun getPortfolio(params: UrlParameter) {
        disposable.add(
            apiService.getUserPortfolio(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<UserPortfolio>() {
                    override fun onSuccess(t: UserPortfolio) {
                        userPortfolio.value = t
                    }

                    override fun onError(e: Throwable) {
                        errorResult.value = e.localizedMessage.orEmpty()
                    }
                })
        )
    }

    fun getTotalPriceOfPortfolio(): Double? {
        return userPortfolio.value?.items?.sumOf { (it.qtyT2 * it.lastPx) }
    }

}