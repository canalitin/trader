package com.cnltn.trader.views.bases

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import com.cnltn.trader.R
import com.cnltn.trader.models.enum.Tag
import com.cnltn.trader.utilities.showMessage

abstract class BaseFragment : Fragment() {

    abstract fun initUI()

    open fun initObservers() {}

    open fun onFailed(message: String) {
        context.showMessage(getString(R.string.msg_title_alert), message)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initObservers()
    }

    fun navigate(navDirections: NavDirections? = null) {
        try {
            navDirections?.let { nd ->
                view?.let { Navigation.findNavController(it).navigate(nd) }
            }
        } catch (e: Exception) {
            Log.e(Tag.APP, "navigate: " + e.localizedMessage)
        }
    }
}