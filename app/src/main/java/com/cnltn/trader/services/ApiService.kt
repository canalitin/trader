package com.cnltn.trader.services

import com.cnltn.trader.models.api.IApi
import com.cnltn.trader.models.enum.Parameter
import com.cnltn.trader.models.model.UrlParameter
import com.cnltn.trader.models.model.User
import com.cnltn.trader.models.model.UserPortfolio
import com.google.gson.GsonBuilder
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiService {

    private val api = Retrofit.Builder()
        .baseUrl(Parameter.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(IApi::class.java)

    fun getUserLoginIn(
        params: UrlParameter
    ): Single<User> {
        return api.getUserLoginIn(
            params.msgType,
            params.customerNo,
            params.username,
            params.password,
            params.accountID,
            params.exchangeID,
            params.outputType
        )
    }

    fun getUserPortfolio(
        params: UrlParameter
    ): Single<UserPortfolio> {
        return api.getUserPortfolio(
            params.msgType,
            params.customerNo,
            params.username,
            params.password,
            params.accountID,
            params.exchangeID,
            params.outputType
        )
    }
}