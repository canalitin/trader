package com.cnltn.trader.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cnltn.trader.R
import com.cnltn.trader.databinding.FragPortfolioRowBinding
import com.cnltn.trader.models.model.Item

class AdpUserPortfolio : RecyclerView.Adapter<AdpUserPortfolio.ViewHolder>() {

    private val items: ArrayList<Item> = arrayListOf()

    class ViewHolder(var view: FragPortfolioRowBinding) : RecyclerView.ViewHolder(view.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<FragPortfolioRowBinding>(
            inflater,
            R.layout.frag_portfolio_row,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.item = items[position]
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItemList(items: ArrayList<Item>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

}