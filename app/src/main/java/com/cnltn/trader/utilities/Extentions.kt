package com.cnltn.trader.utilities

import android.content.Context
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import com.cnltn.trader.R
import com.cnltn.trader.models.enum.Parameter
import java.text.NumberFormat

@BindingAdapter("android:numberformat")
fun numberFormat(textView: TextView, double: Double) {
    textView.text = double.format()
}

@BindingAdapter("android:multiplyFirstNumber", "android:multiplySecondNumber")
fun multiply(textView: TextView, multiplyFirstNumber: Double, multiplySecondNumber: Double) {
    val value = multiplyFirstNumber * multiplySecondNumber
    textView.text = value.format()
}

fun Context?.showMessage(title: String, message: String) {
    this?.let {
        val alertDialog = AlertDialog.Builder(it)
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(R.string.btn_okay) { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }
}

fun Double?.format(): String {
    val numberFormat: NumberFormat = NumberFormat.getNumberInstance()
    numberFormat.maximumFractionDigits = Parameter.MAX_DIGITS
    return numberFormat.format(this ?: 0.0)
}