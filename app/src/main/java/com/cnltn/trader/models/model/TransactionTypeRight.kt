package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class TransactionTypeRight(
    @SerializedName("ExchangeID")
    val exchangeID: Int?,

    @SerializedName("Rights")
    val rights: List<Right>
)