package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class Right(
    @SerializedName("Side")
    val side: Int?,

    @SerializedName("L")
    val l: List<Int>
)