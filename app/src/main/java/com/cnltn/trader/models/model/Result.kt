package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("State")
    val state: Boolean = false,

    @SerializedName("Code")
    val code: Int,

    @SerializedName("Description")
    val description: String,

    @SerializedName("SessionKey")
    val sessionKey: String,

    @SerializedName("Duration")
    val duration: Int,

    @SerializedName("MsgType")
    val msgType: String,

    @SerializedName("Timestamp")
    val timestamp: Any? = null,

    @SerializedName("ClOrdID")
    val clOrdID: String,

    @SerializedName("Reserved")
    val reserved: String,

    @SerializedName("V")
    val v: String
)