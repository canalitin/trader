package com.cnltn.trader.models.api

import com.cnltn.trader.models.enum.Parameter
import com.cnltn.trader.models.model.User
import com.cnltn.trader.models.model.UserPortfolio
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface IApi {

    @GET(Parameter.BASE_URL)
    fun getUserLoginIn(
        @Query("MsgType") msgType: String?,
        @Query("CustomerNo") customerNo: String?,
        @Query("Username") username: String?,
        @Query("Password") password: String?,
        @Query("AccountID") accountID: String?,
        @Query("ExchangeID") exchangeID: Int?,
        @Query("OutputType") outputType: Int?,
    ): Single<User>

    @GET(Parameter.BASE_URL)
    fun getUserPortfolio(
        @Query("MsgType") msgType: String?,
        @Query("CustomerNo") customerNo: String?,
        @Query("Username") username: String?,
        @Query("Password") password: String?,
        @Query("AccountID") accountID: String?,
        @Query("ExchangeID") exchangeID: Int?,
        @Query("OutputType") outputType: Int?,
    ): Single<UserPortfolio>
}