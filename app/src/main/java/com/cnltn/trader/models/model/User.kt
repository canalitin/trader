package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("Result")
    val result: Result,

    @SerializedName("AccountList")
    val accountList: List<String>,

    @SerializedName("DefaultAccount")
    val defaultAccount: String,

    @SerializedName("CustomerID")
    val customerID: String,

    @SerializedName("UserRights")
    val userRights: List<AccountRight>,

    @SerializedName("AccountItems")
    val accountItems: List<AccountItem>,

    @SerializedName("MarketDataToken")
    val marketDataToken: String,

    @SerializedName("CustomerName")
    val customerName: String,

    @SerializedName("ExCode")
    val exCode: Int?,

    @SerializedName("AccountListEx")
    val accountListEx: List<Any?>,

    @SerializedName("LicenceList")
    val licenceList: List<Any?>,

    @SerializedName("DevicePairToken")
    val devicePairToken: String,

    @SerializedName("Statistics")
    val statistics: Any? = null,

    @SerializedName("MatriksID")
    val matriksID: String
)