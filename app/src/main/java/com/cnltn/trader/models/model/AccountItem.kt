package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class AccountItem(
    @SerializedName("AccountID")
    val accountID: String?,

    @SerializedName("ExchangeAccountID")
    val exchangeAccountID: Map<String, String>,

    @SerializedName("AccountRights")
    val accountRights: List<AccountRight>,

    @SerializedName("TransactionTypeRights")
    val transactionTypeRights: List<TransactionTypeRight>,

    @SerializedName("Reserved")
    val reserved: String,

    @SerializedName("AccountIDExtended")
    val accountIDExtended: String
)