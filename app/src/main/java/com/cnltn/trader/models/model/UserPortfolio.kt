package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class UserPortfolio(
    @SerializedName("Result")
    val result: Result,

    @SerializedName("Header")
    val header: List<String>,

    @SerializedName("Item")
    var items: ArrayList<Item>,

    @SerializedName("Other")
    val other: List<Any?>
)