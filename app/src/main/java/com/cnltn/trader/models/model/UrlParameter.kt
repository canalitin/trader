package com.cnltn.trader.models.model

import com.cnltn.trader.models.enum.Parameter
import java.io.Serializable

data class UrlParameter(
    var msgType: String? = null,
    var customerNo: String? = null,
    var username: String? = null,
    var password: String? = null,
    var accountID: String? = null,
    var exchangeID: Int? = null,
    var outputType: Int? = null,
) : Serializable {

    fun setDefaultParameterForLoginIn() {
        msgType = Parameter.MSG_TYPE_A
        customerNo = Parameter.DEFAULT_CUSTOMER_NO
        accountID = Parameter.DEFAULT_ACCOUNT_ID
        exchangeID = Parameter.EXCHANGE_ID
        outputType = Parameter.OUTPUT_TYPE_JSON
    }

    fun setDefaultParameterForPortfolio() {
        msgType = Parameter.MSG_TYPE_AN
        customerNo = Parameter.DEFAULT_CUSTOMER_NO
        exchangeID = Parameter.EXCHANGE_ID
        outputType = Parameter.OUTPUT_TYPE_JSON
    }

}