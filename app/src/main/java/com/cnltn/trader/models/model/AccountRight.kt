package com.cnltn.trader.models.model

import com.google.gson.annotations.SerializedName

data class AccountRight(
    @SerializedName("Code")
    val code: String,

    @SerializedName("Key")
    val key: String,

    @SerializedName("Value")
    val value: String,

    @SerializedName("Timestamp")
    val timestamp: Long,

    @SerializedName("DataType")
    val dataType: Int
)