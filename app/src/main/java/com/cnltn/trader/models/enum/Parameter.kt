package com.cnltn.trader.models.enum

object Parameter {
    const val BASE_URL: String = "https://tbpilot.matriksdata.com/9999/Integration.aspx/"
    const val MSG_TYPE_A = "A"
    const val MSG_TYPE_AN = "AN"
    const val DEFAULT_ACCOUNT_ID = "0"
    const val DEFAULT_CUSTOMER_NO = "0"
    const val EXCHANGE_ID = 4
    const val OUTPUT_TYPE_JSON = 2
    const val MAX_DIGITS = 2
}